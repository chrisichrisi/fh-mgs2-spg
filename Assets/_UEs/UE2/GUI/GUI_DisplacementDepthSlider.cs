﻿using UnityEngine;
using System.Collections;

public class GUI_DisplacementDepthSlider : GUI_FloatSlider {

	// Methods	
	protected override void OnSetup()
	{
		this.shaderParameterName = "_DisplacementDepth";
		this.minValue = 0.01f;
		this.maxValue = 0.1f;
		this.labelDesc = "Displacement Depth";
		
		this.Set(this.GetShaderValue());
	}
}
