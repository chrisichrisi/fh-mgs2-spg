﻿using UnityEngine;
using System.Collections;

public class GUI_RefinementSteps : GUI_IntSlider {

	// Methods
	protected override void OnSetup()
	{
		this.shaderParameterName = "_RefinementSteps";
		this.minValue = 2;
		this.maxValue = 64;
		this.labelDesc = "Refinement Steps";
		
		this.Set(this.GetShaderValue());
	}
}
