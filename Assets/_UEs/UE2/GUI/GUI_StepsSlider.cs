﻿using UnityEngine;
using System.Collections;

public class GUI_StepsSlider : GUI_IntSlider {

	// Methods
	protected override void OnSetup()
	{
		this.shaderParameterName = "_Steps";
		this.minValue = 2;
		this.maxValue = 64;
		this.labelDesc = "Steps";
		
		this.Set(this.GetShaderValue());
	}
}
