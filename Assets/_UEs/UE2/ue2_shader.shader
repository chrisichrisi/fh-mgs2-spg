﻿Shader "SPG/UE2DisplacementShader" {
	Properties {
		_Color ("Main Color", Color) = (1, 1, 1, 0)
		
		_SpecColor ("Specular Material Color", Color) = (1,1,1,1) 
		_Shininess ("Shininess", Float) = 10
		
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_NormalMap ("Normalmap (RGB)", 2D) = "bump" {}
		_DisplacementMap ("Displacement Map (Gray)", 2D) = "white" {}
		
		_Bumpiness ("Bumpiness", float) = 0.0
		
		_Steps("Steps", int) = 10
		_RefinementSteps("Refinement Steps", int) = 5
		_DisplacementDepth("Displacement Depth", float) = 0.08
		
	}
	
	SubShader {
		Pass {
			Tags { "LightMode" = "ForwardBase" }
			CGPROGRAM
				#pragma target 4.0
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"
				
				// ========= DEFINES =========
				#define NO_SPECULAR
				
				// ========= PARAMETERS =========
				float4 _Color;
				float4 _SpecColor;
				float  _Shininess;
				float  _Bumpiness;				
				// Displacement Parameters
				int    _Steps;
				int    _RefinementSteps;
				float  _DisplacementDepth;
				// ========= Textures/Maps =========
				sampler2D _MainTex;
				sampler2D _NormalMap;
				sampler2D _DisplacementMap;
				// ========= Used by Unity to provide offset/tiling =========
				float4 _MainTex_ST;
				float4 _NormalMap_ST;
				float4 _DisplacementMap_ST;
				// ========= Used by Unity to provide Light Color =========
				fixed4 _LightColor0;
							
				
				// ========= VERTEX to FRAGMENT Struct =========
				struct v2f {
					float4 mvpPos : SV_POSITION;
					float2 UV : TEXCOORD0;
					float3 tsLight : TEXCOORD1;
					float3 tsView : TEXCOORD2;
					float3 tsNormal : TEXCOORD3;
				};
				
				
				// ========= VERTEX SHADER =========
				v2f vert(appdata_tan v)	// use appdata_tan to have access to the vertex tangent
				{
					v2f o;
					
					float4 wsPos = mul(v.vertex, _Object2World);
					// ========= NORMAL, TANGENT, BITANGENT =========
					fixed4 normal   = normalize(mul(float4(v.normal, 0.0), _Object2World));
					fixed4 tangent  = normalize(mul(v.tangent,             _Object2World));
					fixed3 binormal = normalize(cross(v.tangent.xyz, v.normal.xyz) * tangent.w);
										
					// If Directional, then w = 0, otherwise 1
					float isNonDirLight = _WorldSpaceLightPos0.w;					
					
					// Calculate light direction and view direction
					float3 lightDir = _WorldSpaceLightPos0.xyz - (wsPos.xyz * isNonDirLight);
					float3 viewDir  = _WorldSpaceCameraPos - wsPos.xyz;
					// Create Matrix: Worldsapce To Tangentspace					
					fixed3x3 ws2tsMatrix = fixed3x3(tangent.xyz, binormal, normal.xyz);
					// ========= SETUP OUTPUT =========
					o.mvpPos = mul(UNITY_MATRIX_MVP, v.vertex);	// Position					
					o.UV     = v.texcoord;						// Texture coordinates
					o.tsNormal = mul(ws2tsMatrix, normal.xyz);	// Normal
					o.tsLight  = mul(ws2tsMatrix, lightDir);	// Light
					o.tsView   = mul(ws2tsMatrix, viewDir);		// View
												
					return o;
				}
				
				// ========= Refinement Displaced UVs =========
				float2 displaceUVsRefinement(float2 originalUV, float2 dUV, float dH, float postHitH)
				{
					// ========= Set Height and UVs before hit =========
					float preHitH = postHitH + dH;
					float2 uv = originalUV.xy + dUV * (1.f - preHitH) / dH;
					// ========= Differences for loop =========
					dH  /= _RefinementSteps; // difference for the height
					dUV /= _RefinementSteps; // difference for the uvs
					// ========= Setup state variables =========
					float prev_hits = 0.0f;		// Used to ignore any other hits
					float hit_h     = 0.0f;		// Stores the height of the hit
		    		float h         = preHitH;	// Loop from preHitH to posHitH
		    		// ========= For Loop =========
		    		for (int it = 0; it < _RefinementSteps; ++it)
		    		{
		     			h  -= dH;
		     			uv += dUV;
		     			float tex_h = tex2D(_DisplacementMap, TRANSFORM_TEX(uv, _DisplacementMap)).r;
		     			float isFirstHit = saturate((tex_h - h - (prev_hits)) * 999999999.f);
		     			hit_h     += isFirstHit * h;
		     			prev_hits += isFirstHit;
		    		}
		    		// ========= When no hit found, set hit_h to postHitH =========
		    		hit_h = lerp(postHitH, hit_h, min(hit_h * 999999999.f, 1.f));
		    		// ========= Return displaced UVs =========
		    		return originalUV.xy + dUV * (1.0f - hit_h) / dH;
		    	}
				
				// ========= Displaced UVs =========
				float2 displaceUVs(float2 originalUV, float3 tsEyeVec)
				{
					float2 uv = originalUV;	// Set uv to original UVs
					// ========= Differences for loop =========
					float dH = 1.f / (_Steps); // difference for the height
					float2 dUV = -tsEyeVec.xy * (_DisplacementDepth / (_Steps)); // difference for the uvs
					// ========= Setup state variables =========
					float prev_hits = 0.0f;	// Used to ignore any other hits
					float hit_h = 0.0f;		// Stores the height of the hit
		    		float h = 1.0f;			// Loop from 1 to 0
		    		// ========= For Loop =========
		    		for (int it = 0; it < _Steps; ++it)
		    		{
		     			h  -= dH;
		     			uv += dUV;
		     			float tex_h = tex2D(_DisplacementMap, TRANSFORM_TEX(uv, _DisplacementMap)).r;
		    	 		float isFirstHit = saturate((tex_h - h - prev_hits) * 4999999);
		     			hit_h     += isFirstHit * h;
		     			prev_hits += isFirstHit;
		    		}
		    		// ========= Refine and return =========
		    		return displaceUVsRefinement(originalUV, dUV, dH, hit_h);
				}
				
				// ========= FRAGMENT SHADER =========
				fixed4 frag(v2f i) : COLOR
				{
					// ========= Normalize vectors =========
					i.tsLight = normalize(i.tsLight);
					i.tsView  = normalize(i.tsView);					
					// ========= Get displaced UVs =========
					i.UV = displaceUVs(i.UV, i.tsView);
					// ========= SAMPLE FROM TEXTURES =========
					fixed4 texCol = tex2D(_MainTex, TRANSFORM_TEX(i.UV, _MainTex));	// read color from _Main
					fixed4 normalFromMap = tex2D(_NormalMap, TRANSFORM_TEX(i.UV, _NormalMap)); // Read normal from _Normal
					// ========= UNPACK NORMAL FROM NORMAL MAP =========
					// Unity stores x and y from normal in a and g, if texture type is "Normal Map"
					fixed3 bumpNormal;
					bumpNormal.xy = normalFromMap.ag * 2 - 1; // Expand from [0,1] to [-1,1]
					bumpNormal.z = sqrt(1 - saturate(dot(bumpNormal.xy, bumpNormal.xy)));	// Normalvector has length 1, so the z component can be calulcated from x and y
					bumpNormal = bumpNormal * _Bumpiness;	// Add Bumpiness factor
					// ========= CALCULATE LIGHTING VARIABLES =========
					i.tsNormal = normalize(i.tsNormal + bumpNormal);	// Combined Normal
					float nl = dot(i.tsNormal, i.tsLight);				// N dot L
					fixed shadow = saturate(4 * nl); 					// Compute self shadowing term
					//float att = dot(i.Attenuation, i.Attenuation);	// Compute Light Attenuation
					// ========= CALCULATE COLORS =========
					// Ambient color
					fixed4 ambientCol = saturate(UNITY_LIGHTMODEL_AMBIENT * _Color);
					// Diffuse Color
					fixed4 diffuseCol = _Color * _LightColor0 * max(0.0, nl);
					#ifndef NO_SPECULAR
					// Specular Color
					fixed3 reflect = normalize(2 * nl * i.tsNormal - i.tsLight);					
					fixed4 specularCol = _SpecColor * _LightColor0 * pow(saturate(dot(reflect, i.tsView)), _Shininess);
					#endif
					// ========= CALCULATE FINAL COLOR =========
					fixed4 col = {0.0f, 0.0f, 0.0f, 0.0f};
					col += ambientCol;
					#ifdef NO_SPECULAR
					//col += (shadow * (diffuseCol)) * (1 - att);
					col += (shadow * (diffuseCol));
					#else
					//col += (shadow * (diffuseCol + specularCol)) * (1 - att);
					col += (shadow * (diffuseCol + specularCol));
					#endif
					col *= texCol;
					
					return col;
				}
			ENDCG
		}
		/*
		Pass {
			// Pass for additional light sources
			Tags { "LightMode" = "ForwardAdd" }
			Blend One One	// Additive Blending
			
			CGPROGRAM
				#pragma target 4.0
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"
				
				// ========= DEFINES =========
				#define NO_SPECULAR
				
				// ========= PARAMETERS =========
				float4 _Color;
				float4 _SpecColor;
				float  _Shininess;
				float  _Bumpiness;				
				// Displacement Parameters
				int    _Steps;
				int    _RefinementSteps;
				float  _DisplacementDepth;
				// ========= Textures/Maps =========
				sampler2D _MainTex;
				sampler2D _NormalMap;
				sampler2D _DisplacementMap;
				// ========= Used by Unity to provide offset/tiling =========
				float4 _MainTex_ST;
				float4 _NormalMap_ST;
				float4 _DisplacementMap_ST;
				// ========= Used by Unity to provide Light Color =========
				fixed4 _LightColor0;
							
				
				// ========= VERTEX to FRAGMENT Struct =========
				struct v2f {
					float4 mvpPos : SV_POSITION;
					float2 UV : TEXCOORD0;
					float3 tsLight : TEXCOORD1;
					float3 tsView : TEXCOORD2;
					float3 tsNormal : TEXCOORD3;
				};
				
				
				// ========= VERTEX SHADER =========
				v2f vert(appdata_tan v)	// use appdata_tan to have access to the vertex tangent
				{
					v2f o;
					
					float4 wsPos = mul(v.vertex, _Object2World);
					// ========= NORMAL, TANGENT, BITANGENT =========
					fixed4 normal   = normalize(mul(float4(v.normal, 0.0), _Object2World));
					fixed4 tangent  = normalize(mul(v.tangent,             _Object2World));
					fixed3 binormal = normalize(cross(v.tangent.xyz, v.normal.xyz) * tangent.w);
										
					// If Directional, then w = 0, otherwise 1
					float isNonDirLight = _WorldSpaceLightPos0.w;					
					
					// Calculate light direction and view direction
					float3 lightDir = _WorldSpaceLightPos0.xyz - (wsPos.xyz * isNonDirLight);
					float3 viewDir  = _WorldSpaceCameraPos - wsPos.xyz;
					// Create Matrix: Worldsapce To Tangentspace					
					fixed3x3 ws2tsMatrix = fixed3x3(tangent.xyz, binormal, normal.xyz);
					// ========= SETUP OUTPUT =========
					o.mvpPos = mul(UNITY_MATRIX_MVP, v.vertex);	// Position					
					o.UV     = v.texcoord;						// Texture coordinates
					o.tsNormal = mul(ws2tsMatrix, normal.xyz);	// Normal
					o.tsLight  = mul(ws2tsMatrix, lightDir);	// Light
					o.tsView   = mul(ws2tsMatrix, viewDir);		// View
												
					return o;
				}
				
				// ========= Refinement Displaced UVs =========
				float2 displaceUVsRefinement(float2 originalUV, float2 dUV, float dH, float postHitH)
				{
					// ========= Set Height and UVs before hit =========
					float preHitH = postHitH + dH;
					float2 uv = originalUV.xy + dUV * (1.f - preHitH) / dH;
					// ========= Differences for loop =========
					dH  /= _RefinementSteps; // difference for the height
					dUV /= _RefinementSteps; // difference for the uvs
					// ========= Setup state variables =========
					float prev_hits = 0.0f;		// Used to ignore any other hits
					float hit_h     = 0.0f;		// Stores the height of the hit
		    		float h         = preHitH;	// Loop from preHitH to posHitH
		    		// ========= For Loop =========
		    		for (int it = 0; it < _RefinementSteps; ++it)
		    		{
		     			h  -= dH;
		     			uv += dUV;
		     			float tex_h = tex2D(_DisplacementMap, TRANSFORM_TEX(uv, _DisplacementMap)).r;
		     			float isFirstHit = saturate((tex_h - h - (prev_hits)) * 999999999.f);
		     			hit_h     += isFirstHit * h;
		     			prev_hits += isFirstHit;
		    		}
		    		// ========= When no hit found, set hit_h to postHitH =========
		    		hit_h = lerp(postHitH, hit_h, min(hit_h * 999999999.f, 1.f));
		    		// ========= Return displaced UVs =========
		    		return originalUV.xy + dUV * (1.0f - hit_h) / dH;
		    	}
				
				// ========= Displaced UVs =========
				float2 displaceUVs(float2 originalUV, float3 tsEyeVec)
				{
					float2 uv = originalUV;	// Set uv to original UVs
					// ========= Differences for loop =========
					float dH = 1.f / (_Steps); // difference for the height
					float2 dUV = -tsEyeVec.xy * (_DisplacementDepth / (_Steps)); // difference for the uvs
					// ========= Setup state variables =========
					float prev_hits = 0.0f;	// Used to ignore any other hits
					float hit_h = 0.0f;		// Stores the height of the hit
		    		float h = 1.0f;			// Loop from 1 to 0
		    		// ========= For Loop =========
		    		for (int it = 0; it < _Steps; ++it)
		    		{
		     			h  -= dH;
		     			uv += dUV;
		     			float tex_h = tex2D(_DisplacementMap, TRANSFORM_TEX(uv, _DisplacementMap)).r;
		    	 		float isFirstHit = saturate((tex_h - h - prev_hits) * 4999999);
		     			hit_h     += isFirstHit * h;
		     			prev_hits += isFirstHit;
		    		}
		    		// ========= Refine and return =========
		    		return displaceUVsRefinement(originalUV, dUV, dH, hit_h);
				}
				
				// ========= FRAGMENT SHADER =========
				fixed4 frag(v2f i) : COLOR
				{
					// ========= Normalize vectors =========
					i.tsLight = normalize(i.tsLight);
					i.tsView  = normalize(i.tsView);					
					// ========= Get displaced UVs =========
					i.UV = displaceUVs(i.UV, i.tsView);
					// ========= SAMPLE FROM TEXTURES =========
					fixed4 texCol = tex2D(_MainTex, TRANSFORM_TEX(i.UV, _MainTex));	// read color from _Main
					fixed4 normalFromMap = tex2D(_NormalMap, TRANSFORM_TEX(i.UV, _NormalMap)); // Read normal from _Normal
					// ========= UNPACK NORMAL FROM NORMAL MAP =========
					// Unity stores x and y from normal in a and g, if texture type is "Normal Map"
					fixed3 bumpNormal;
					bumpNormal.xy = normalFromMap.ag * 2 - 1; // Expand from [0,1] to [-1,1]
					bumpNormal.z = sqrt(1 - saturate(dot(bumpNormal.xy, bumpNormal.xy)));	// Normalvector has length 1, so the z component can be calulcated from x and y
					bumpNormal = bumpNormal * _Bumpiness;	// Add Bumpiness factor
					// ========= CALCULATE LIGHTING VARIABLES =========
					i.tsNormal = normalize(i.tsNormal + bumpNormal);	// Combined Normal
					float nl = dot(i.tsNormal, i.tsLight);				// N dot L
					fixed shadow = saturate(4 * nl); 					// Compute self shadowing term
					//float att = dot(i.Attenuation, i.Attenuation);	// Compute Light Attenuation
					// ========= CALCULATE COLORS =========
					// Ambient color
					fixed4 ambientCol = saturate(UNITY_LIGHTMODEL_AMBIENT * _Color);
					// Diffuse Color
					fixed4 diffuseCol = _Color * _LightColor0 * max(0.0, nl);
					#ifndef NO_SPECULAR
					// Specular Color
					fixed3 reflect = normalize(2 * nl * i.tsNormal - i.tsLight);					
					fixed4 specularCol = _SpecColor * _LightColor0 * pow(saturate(dot(reflect, i.tsView)), _Shininess);
					#endif
					// ========= CALCULATE FINAL COLOR =========
					fixed4 col = {0.0f, 0.0f, 0.0f, 0.0f};
					#ifdef NO_SPECULAR
					//col += (shadow * (diffuseCol)) * (1 - att);
					col += (shadow * (diffuseCol));
					#else
					//col += (shadow * (diffuseCol + specularCol)) * (1 - att);
					col += (shadow * (diffuseCol + specularCol));
					#endif
					col *= texCol;
					
					return col;
				}
			ENDCG
		}
		*/
	} 
}
