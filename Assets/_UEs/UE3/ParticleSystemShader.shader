﻿Shader "SPG/UE3ParticleSystemShader" {
	Properties  {
		_MainTex("Main Texture", 2D) = "white" {}
	}


	SubShader {
		//Tags { "RenderType"="Opaque" }

		Pass {
			Tags { "LightMode" = "ForwardBase" }

			ZWrite Off 
			ZTest Always 
			Cull Off 
			Fog { Mode Off }
    		Blend one one


			CGPROGRAM
				#pragma enable_d3d11_debug_symbols
				
				#pragma vertex VS_Main  
				#pragma geometry GS_Main
				#pragma fragment FS_Main 

				#include "UnityCG.cginc"
	
				// ========= Structs =========
				struct v2g
				{
					float4 mvpPos : POSITION;
					int id : TEXCOORD0;
				};
				
				struct g2f
				{
					float4	pos		: POSITION;
					float2  uv		: TEXCOORD0;
					int		id		: TEXCOORD1;
				};


				struct ParticleData
				{
					int type;
					float lifetime;
				};

				// ========= BUFFERS =========
				StructuredBuffer<float3> _Positions;
				StructuredBuffer<ParticleData> _ParticleData;

				// ========= GLOBALS =========
				sampler2D _MainTex;


					
				// ========= VERTEX SHADER =========
				v2g VS_Main(uint id : SV_VertexID)
				{
					v2g OUT = (v2g)0;
					
					float3 worldPos = _Positions[id].xyz;
					OUT.mvpPos = mul(UNITY_MATRIX_VP, float4(worldPos,1.0f));
					OUT.id = id;
	
					return OUT;
				}

				// ========= GEOMETRY SHADER =========
				[maxvertexcount(4)]
				void GS_Main(point v2g IN[1], inout TriangleStream<g2f> outStream)
				{
					int id = IN[0].id;
					ParticleData pd = _ParticleData[id];
					if(pd.type == 1)		{ return; }	// This is a emitter particle
					if(pd.lifetime <= 0.0f) { return; } // This is a dead particle
					

					float _Size = 0.25f;

					float dx = _Size;
					float dy = _Size * _ScreenParams.x / _ScreenParams.y;

					g2f OUT;
					OUT.pos = IN[0].mvpPos + float4(-dx, dy,0,0); OUT.uv=float2(0,0); OUT.id=id; outStream.Append(OUT);
					OUT.pos = IN[0].mvpPos + float4( dx, dy,0,0); OUT.uv=float2(1,0); OUT.id=id; outStream.Append(OUT);
					OUT.pos = IN[0].mvpPos + float4(-dx,-dy,0,0); OUT.uv=float2(0,1); OUT.id=id; outStream.Append(OUT);
					OUT.pos = IN[0].mvpPos + float4( dx,-dy,0,0); OUT.uv=float2(1,1); OUT.id=id; outStream.Append(OUT);
					outStream.RestartStrip();
				}
	
				// ========= FRAGMENT SHADER =========
				float4 FS_Main(g2f IN) : COLOR
				{
					// DEBUG
					/*
					float3 p = saturate(_Positions[IN.id].xyz);
					return float4(p, 1.0f);
					*/


					return tex2D(_MainTex, IN.uv);
				}
	
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
