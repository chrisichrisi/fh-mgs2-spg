﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.Collections.Generic;


public enum ParticleType
{
    Normal, Emitter
}

public struct ParticleData
{
    //public int type;
    public ParticleType Type;
    public float Lifetime;
}


public class GPUParticleSystem : MonoBehaviour
{
    #region Static
    private static readonly string KERNEL_NAME = "CSMain";

    private static readonly string CS_LABEL_BUFFER = "RWSBParticlesOut";
    private static readonly string CS_LABEL_META_BUFFER = "RWSBParticlesMetaOut";
    private static readonly string CS_LABEL_MAXPARTICLES = "MAX_PARTICLES";
    private static readonly string CS_LABEL_DT = "DT";
    private static readonly string CS_LABEL_GRAVITY = "GRAVITY";

    private static readonly string CS_LABEL_PT_EMITTER = "PT_EMITTER";
    private static readonly string CS_LABEL_PT_NORMAL = "PT_NORMAL";

    private static readonly string CS_LABEL_DPI_BUFFER = "RWSBDeadParticleIndices";



    private static readonly string CS_LABEL_LIFETIME_MIN = "LIFETIME_MIN";
    private static readonly string CS_LABEL_LIFETIME_MAX = "LIFETIME_MAX";

    // PARAMETERS
    private static readonly int CS_NUM_THREADS_PER_GROUP_X = 8;

    private static readonly float GRAVITY = 1.0f;
    private static readonly float MIN_SIZE = 0.1f;
    private static readonly float MAX_SIZE = 0.3f;
    #endregion // Static


    #region Fields
    #region Editor Fields
    public int MAX_PARTICLES = 50;

    public float minLifetime;
    public float maxLifetime;



    public ComputeShader particleComputeShader;
    public Material particleMaterial;
    #endregion // Editor Fields


    #region ComputeShader Fields
    private int _kernel;
    private int _threadGroupsX;

    // Compute Buffers
    private ComputeBuffer _cbufRW;
    private ComputeBuffer _cbufParticleData;
    private ComputeBuffer _cbufDeadParticles;
    #endregion // ComputeShader Fields

    #endregion // Fields


    #region Properties
    #endregion // Properties


    #region Methods
    /// <summary>
    /// Initializes the material for the particles
    /// </summary>
    void InitParticleMaterial()
    {
        particleMaterial.SetBuffer("_Positions", this._cbufRW);
        particleMaterial.SetBuffer("_ParticleData", this._cbufParticleData);
    }


    /// <summary>
    /// Renders the particles onto the screen
    /// </summary>
    public void RenderParticles()
    {
        particleMaterial.SetPass(0);
        Graphics.DrawProcedural(MeshTopology.Points, MAX_PARTICLES);
    }





    #region Compute Shader Methods
    /// <summary>
    /// Initializes all the components needed for the compute shader
    /// </summary>
    private void InitComputeShader()
    {
        _threadGroupsX = Mathf.CeilToInt((float)MAX_PARTICLES / (float)CS_NUM_THREADS_PER_GROUP_X);
        this._kernel = this.particleComputeShader.FindKernel(KERNEL_NAME);
        // Set static compute shader data
        this.particleComputeShader.SetInt(CS_LABEL_MAXPARTICLES, MAX_PARTICLES);
        this.particleComputeShader.SetFloat(CS_LABEL_GRAVITY, GRAVITY);

        // Set Particle Types
        this.particleComputeShader.SetInt(CS_LABEL_PT_EMITTER, (int)ParticleType.Emitter);
        this.particleComputeShader.SetInt(CS_LABEL_PT_NORMAL, (int)ParticleType.Normal);

        // Set Lifetime
        this.particleComputeShader.SetFloat(CS_LABEL_LIFETIME_MIN, this.minLifetime);
        this.particleComputeShader.SetFloat(CS_LABEL_LIFETIME_MAX, this.maxLifetime);


        this.InitComputeBuffers();
    }

    /// <summary>
    /// Initializes the buffers used by the compute shader
    /// </summary>
    private void InitComputeBuffers()
    {
        // Create Buffers
        _cbufRW = new ComputeBuffer(MAX_PARTICLES, sizeof(float) * 3);
        _cbufParticleData = new ComputeBuffer(MAX_PARTICLES, System.Runtime.InteropServices.Marshal.SizeOf(typeof(ParticleData)));
        _cbufDeadParticles = new ComputeBuffer(MAX_PARTICLES, sizeof(int));
        
        // Init RW
        Vector3[] pos = new Vector3[MAX_PARTICLES];
        pos[0] = this.transform.position;

        // Init Test
        ParticleData[] particleData = new ParticleData[MAX_PARTICLES];
        //tests[0].type = 1;
        particleData[0].Type = ParticleType.Emitter;
        particleData[0].Lifetime = -1.0f;


        // Init Dead Particles
        int curDPIIndex = 0;
        int[] deadParticleIndices = new int[MAX_PARTICLES];
        for(int i = 0; i < particleData.Length; ++i)
        {
            if (particleData[i].Type != ParticleType.Emitter && particleData[i].Lifetime <= 0.0f)
            {
                ++curDPIIndex;
                deadParticleIndices[curDPIIndex] = i;

                pos[i] = new Vector3(-1.0f, -1.0f, -1.0f);
            }
        }
        deadParticleIndices[0] = curDPIIndex;

        // Set Data
        _cbufRW.SetData(pos);
        _cbufParticleData.SetData(particleData);
        _cbufDeadParticles.SetData(deadParticleIndices);

        // Set Buffers
        this.particleComputeShader.SetBuffer(this._kernel, CS_LABEL_BUFFER, _cbufRW);
        this.particleComputeShader.SetBuffer(this._kernel, CS_LABEL_META_BUFFER, _cbufParticleData);
        this.particleComputeShader.SetBuffer(this._kernel, CS_LABEL_DPI_BUFFER, _cbufDeadParticles);
    }
    
    /// <summary>
    /// Execute the kernel on the compute shader
    /// </summary>
    private void DispatchKernel()
    {
        // Set Shader Parameters
        this.particleComputeShader.SetFloat(CS_LABEL_DT, Time.deltaTime);
        // Dispatch Compute Shader
        this.particleComputeShader.Dispatch(this._kernel, this._threadGroupsX, 1, 1);
        //this.PrintBufferData(); // Print
    }
    #endregion // Compute Shader Methods





    /// <summary>
    /// Sets the position of the emitter
    /// </summary>
    /// <param name="newOrigin"></param>
    private void SetPSEmitter(Vector3 newOrigin)
    {
        // Read data from buffer
        Vector3[] pos = new Vector3[MAX_PARTICLES];
        _cbufRW.GetData(pos);
        // Write new origin
        pos[0] = newOrigin;
        _cbufRW.SetData(pos);
        // Also apply the point to the transform
        this.transform.position = newOrigin;
    }

    /// <summary>
    /// Checks if the position of the emitter particle need to be reset
    /// </summary>
    private void CheckPSEmitterReset()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 100))
            {
                this.SetPSEmitter(hit.point);
            }
        }
    }


    
    #region Script Methods
    /// <summary>
    /// Use this for initialization
    /// </summary>
	void Start () 
    {
        this.InitComputeShader();
        this.InitParticleMaterial();
	}
	
    /// <summary>
    /// Update is called once per frame
    /// </summary>
	void Update ()
    {
        this.CheckPSEmitterReset();
        this.DispatchKernel();
    }
    
    /// <summary>
    /// Called when the script will get destroyed
    /// </summary>
    void OnDestroy()
    {
        // Release Buffer
        this._cbufRW.Release();
        this._cbufParticleData.Release();
        this._cbufDeadParticles.Release();
    }
    #endregion //Script Methods

    
    #region Helper Methods
    void PrintBufferData()
    {
        // Read Data from buffer
        Vector3[] pos = new Vector3[MAX_PARTICLES];
        ParticleData[] particleDatas = new ParticleData[MAX_PARTICLES];
        _cbufRW.GetData(pos);
        _cbufParticleData.GetData(particleDatas);

        // Print Data
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < pos.Length; ++i)
        {
            if (particleDatas[i].Type != ParticleType.Emitter && particleDatas[i].Lifetime <= 0) { continue; }
            sb = new StringBuilder();

            sb.Append("Particle ").Append(i).Append(" (").Append(particleDatas[i].Type).AppendLine(")");
            if (particleDatas[i].Type != ParticleType.Emitter)
                sb.Append("Lifetime: ").AppendLine(particleDatas[i].Lifetime.ToString());
            sb.Append(pos[i]);
            Debug.Log(sb.ToString());
        }
    }
    #endregion // Helper Methods
    #endregion // Methods
}
