﻿using UnityEngine;
using System.Collections;

public class PSInvoker : MonoBehaviour
{
    #region Fields
    private GPUParticleSystem _ps;
    #endregion // Fields


    #region Methods
    /// <summary>
    /// Use this for initialization
    /// </summary>
	void Start () 
    {
        var go = GameObject.FindGameObjectWithTag("ParticleSystem");
        _ps = go.GetComponent<GPUParticleSystem>();

        if (_ps == null)
            Debug.Log("GPU Particle System not found");
	}
	
    /// <summary>
    /// Called after Scene is rendered
    /// </summary>
    void OnPostRender()
    {
        // Render Particle System
        _ps.RenderParticles();
    }
    #endregion // Methods
}
