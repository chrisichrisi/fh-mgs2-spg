﻿Shader "SPG/SoftShadower" {
	Properties {
		_ShadowMap("Shadow M", 2D) = "white"{}
		_Bias ("Bias", Range(0, 0.1)) = 0.01
		
		_PCFSteps ("PCF Steps", int) = 4
		_PCFStepSize ("PCF Step Size", float) = 1.0
		
		_MainTex ("Base (RGB)", 2D) = "white" {}
		
	}
	SubShader {
		Pass {
			Tags { "LightMode" = "ForwardBase" }
			CGPROGRAM
				//#pragma target 4.0
				#pragma target 4.0
				#pragma vertex VS_Main
				#pragma fragment FS_Main
				#include "UnityCG.cginc"
				
				
				// ========= PARAMETERS =========
				const uniform float _Bias;
				const float _PCFSteps;
				const float _PCFStepSize;
				// ========= Textures / Shadow Maps =========
				sampler2D _MainTex;
				sampler2D _ShadowMap;
				
				// ========= Light View/Projection =========
				const uniform float4x4 _Light_Matrix_View;
   				const uniform float4x4 _Light_Matrix_Proj;
				// ========= ShadowMap Dimensions =========
   				const uniform float _InverseShadowmapWidth;
   				const uniform float _InverseShadowmapHeight;
				
				// ========= Used by Unity to provide offset/tiling =========
				float4 _MainTex_ST;
				
				// ========= VERTEX to FRAGMENT Struct =========
				struct v2f {
					float4 mvpPos : SV_POSITION;
					float2 UV : TEXCOORD0;
					float3 wsPos : TEXCOORD1;
					float3 wsNormal : TEXCOORD2;
					float4 lvpPos : TEXCOORD3;
				};
				
				
				// ========= VERTEX SHADER =========
				v2f VS_Main(appdata_base v)
				{
					v2f o;
					float4 worldPosition;
					// Change the position vector to be 4 units for proper matrix calculations.
    				//v.vertex.w = 1.0f;

					// Calculate the position of the vertex against the world, view, and projection matrices.
					o.mvpPos = mul(UNITY_MATRIX_MVP, v.vertex);	// Position
					// Calculate the position of the vertex in the world.
					o.wsPos = mul(_Object2World , v.vertex).xyz;					
					// Store the texture coordinates for the pixel shader.
				    o.UV = v.texcoord;				    
					// Calculate the normal vector against the world matrix only.
				    o.wsNormal = mul(v.normal, (float3x3)_Object2World);					
				    // Normalize the normal vector.
				    o.wsNormal = normalize(o.wsNormal);				    
					// Calculate the position of the vertice as viewed by the light sources.
					float4 lvPos = float4(o.wsPos, 1.0f);
					lvPos = mul(_Light_Matrix_View, lvPos);
					lvPos = mul(_Light_Matrix_Proj, lvPos);
					o.lvpPos = lvPos;
					
					return o;
				}
				
				
				// ========= PCF Kernel =========
				float PCFKernel(const float2 projectTexCoord, float lvpPosZ)
				{
					float depthValue, pcfSampleOffsetX, pcfSampleOffsetY;
					float intensity = 0.0f;
					
					static const float pcfEnd = (_PCFSteps / 2.0f) + (_PCFStepSize / 2.0f);
					static const float pcfStart = -pcfEnd;					
					static const float2 shadowMapDim = float2(_InverseShadowmapWidth, _InverseShadowmapHeight);
					
					float2 pcfSampleOffset = float2(pcfStart, pcfStart);
					float4 depthSampleUV = float4(pcfStart, pcfStart, 0.0f, 1.0f);
					
					for(int stepX = 0; stepX < _PCFSteps; ++stepX)
					{
						pcfSampleOffset.x += _PCFStepSize;
						pcfSampleOffset.y = pcfStart;						
						for(int stepY = 0; stepY < _PCFSteps; ++stepY)
						{
							pcfSampleOffset.y += _PCFStepSize;
							depthSampleUV.xy = projectTexCoord.xy + pcfSampleOffset * shadowMapDim;
							depthValue = tex2Dlod(_ShadowMap, depthSampleUV);
							intensity +=  1.f - saturate((lvpPosZ - depthValue - _Bias) * 100000.f);
						}
					}
					return intensity / ((float)_PCFSteps * _PCFSteps);
				}
				
				// ========= FRAGMENT SHADER =========
				float4 FS_Main(v2f i) : COLOR
				{
				    float lightIntensity;

					// Set the default output color to the ambient light value for all pixels.
    				float4 color = saturate(UNITY_LIGHTMODEL_AMBIENT);
    				
    				// Calculate the projected texture coordinates.
					i.lvpPos = i.lvpPos / 2.0f + 0.5f;
					float2 projectTexCoord = i.lvpPos.xy;
					
					// Determine if the projected coordinates are in the 0 to 1 range.  If so then this pixel is in the view of the light.
					if((saturate(projectTexCoord.x) == projectTexCoord.x) && (saturate(projectTexCoord.y) == projectTexCoord.y))
					{
						float intensity = 0.f;											
						// PCF
						intensity = PCFKernel(projectTexCoord, i.lvpPos.z);
						
						// Calculate the amount of light on this pixel.
						float lightDir = normalize(_WorldSpaceLightPos0.xyz);
						lightIntensity = dot(i.wsNormal, lightDir);
						lightIntensity *= intensity;
						
						// Determine the final diffuse color based on the diffuse color and the amount of light intensity.
						// Clamp intensity
						lightIntensity = saturate(lightIntensity);				
						color += lightIntensity;
					}
    				
    				// Saturate the final light color.
					color = saturate(color);
					// Sample the pixel color from the texture using the sampler at this texture coordinate location.
					float4 textureColor = tex2D(_MainTex, TRANSFORM_TEX(i.UV, _MainTex)); // read color from _Main
					// Combine the light and texture color.
					color = color * textureColor;
    				
					return color;
				}
				
				
			ENDCG
		}
	}
	//FallBack "Diffuse"
}
