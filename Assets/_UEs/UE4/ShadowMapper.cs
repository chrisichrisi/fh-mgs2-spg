﻿using UnityEngine;
using System.Collections;

public class ShadowMapper : MonoBehaviour
{

	public Shader _ShadowMapShader;
	public RenderTexture _ShadowMap;
	
	public Camera _LightCam;
	// Labels to bind in shader
	private string _Shader_Label_LMV = "_Light_Matrix_View";
	private string _Shader_Label_LMP = "_Light_Matrix_Proj";
	private string _InverseShadowMapWidthName = "_InverseShadowmapWidth";	
	private string _InverseShadowMapHeightName = "_InverseShadowmapHeight";




	void Start()
	{
		Shader.SetGlobalFloat(_InverseShadowMapWidthName, 1f / (float)_ShadowMap.width);
		Shader.SetGlobalFloat(_InverseShadowMapHeightName, 1f / (float)_ShadowMap.height);
	}



	void OnPreRender()
	{
		Shader.SetGlobalMatrix(_Shader_Label_LMV, _LightCam.worldToCameraMatrix);
		Shader.SetGlobalMatrix(_Shader_Label_LMP, _LightCam.projectionMatrix);
		_LightCam.RenderWithShader(_ShadowMapShader, "");
	}
}
