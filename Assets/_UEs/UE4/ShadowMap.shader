﻿Shader "SPG/Shadow Map" {
	Properties {
	}
	SubShader {
		Pass {
			CULL OFF

			CGPROGRAM

			#pragma vertex VS_Main
			#pragma fragment FS_Main

			struct unity2v{
				float4 vertex : POSITION;
			};

			struct v2f{
				float4 mvpPos : SV_POSITION;
			};

			v2f VS_Main(unity2v v) {
				v2f o;
				o.mvpPos = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			} 

			float4 FS_Main(v2f i) : COLOR
			{
				return float4(i.mvpPos.z, i.mvpPos.z, i.mvpPos.z, 1.0f);
			}

			ENDCG
		}
	}
}