﻿using UnityEngine;
using System.Collections;

public class GUI_SelfShadowing : GUIBase<bool> {
	// Exposed Members

	
	
	// Methods
	protected override void OnSetup()
	{
		this.labelDesc = "Self Shadowing Off";
		this.Set(this.ShaderMaterial.GetInt("_SelfShadowingOff") != 0);
	}

	
	protected override bool DrawGui()
	{
		return GUI.Toggle (this.position, this.value, "On / Off");
	}

	protected override void OnSetValue()
	{
		int val = this.value ? 1 : 0;
		this.ShaderMaterial.SetInt ("_SelfShadowingOff", val);
	}
	
}
