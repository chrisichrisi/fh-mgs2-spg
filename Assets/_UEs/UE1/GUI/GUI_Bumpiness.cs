﻿using UnityEngine;
using System.Collections;

public class GUI_Bumpiness : GUI_FloatSlider {

	// Methods
	protected override void OnSetup()
	{
		this.shaderParameterName = "_Bumpiness";
		this.minValue = -5.0f;
		this.maxValue = 5.0f;
		this.labelDesc = "Bumpiness";

		this.Set(this.GetShaderValue());
	}
}
