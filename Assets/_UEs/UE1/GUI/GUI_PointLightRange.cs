﻿using UnityEngine;
using System.Collections;

public class GUI_PointLightRange : GUI_FloatSlider {

	// Methods
	protected override void OnUpdate() 
	{ 
		this.SetIfNew(this.ShaderLight.range);
	}
		
	protected override void OnSetup()
	{
		this.shaderParameterName = "_LightRange";
		this.minValue = 0.1f;
		this.maxValue = 10.0f;
		this.labelDesc = "Light Range";
		
		this.Set(this.GetShaderValue());
	}

	protected override void OnSetValue()
	{
		base.OnSetValue ();
		this.ShaderLight.range = this.value;
	}
}
