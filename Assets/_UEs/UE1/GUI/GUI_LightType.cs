﻿using UnityEngine;
using System.Collections;

public class GUI_LightType : GUIBase<LightType> {

	// Private Members
	GUI_PointLightRange gui_plr;

	// Methods
	protected override void OnSetup()
	{
		this.gui_plr = this.GetComponent<GUI_PointLightRange> ();

		this.labelDesc = "Light Type";
		this.label = "Light Type: ";
		this.Set(this.ShaderLight.type);
	}
	
	private LightType GetOtherLightType(LightType type)
	{
		if(type == LightType.Directional)
			return LightType.Point;
		return LightType.Directional;
	}
	
	protected override LightType DrawGui()
	{
		bool val;
		string typeDesc = "Point Light";
		
		if(this.value == LightType.Point)
			typeDesc = "Directional Light";
			
		val = GUI.Button(this.position, typeDesc);
		if(val)
		{
			return this.GetOtherLightType(this.value);
		}
		return this.value;
	}
	
	protected override void OnSetValue()
	{
		this.ShaderLight.type = this.value;
		this.gui_plr.enabled = (this.value == LightType.Point);
		
	}
}
