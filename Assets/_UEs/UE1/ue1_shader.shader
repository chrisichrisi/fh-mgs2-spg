﻿Shader "SPG/UE1Shader" {
	Properties {
		_Color ("Main Color", Color) = (1, 1, 1, 0)
		
		_SpecColor ("Specular Material Color", Color) = (1,1,1,1) 
		_Shininess ("Shininess", float) = 10
		
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_NormalMap ("Normalmap (RGB)", 2D) = "bump" {}
		_Bumpiness ("Bumpiness", float) = 0.0
		_SelfShadowingOff ("Self Shadowing Off", int) = 0
		_LightRange ("Light Range", float) = 10.0
	}
	
	SubShader {
		Pass {
			Tags { "LightMode" = "ForwardBase" }
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"
				
				// ========= DEFINES =========
				#define NO_SPECULAR
				
				// ========= PARAMETERS =========
				fixed4 _Color;
				fixed4 _SpecColor;
				float  _Shininess;
				float  _Bumpiness;
				int    _SelfShadowingOff;
				float  _LightRange;
				// ========= Textures/Maps =========
				sampler2D _MainTex;
				sampler2D _NormalMap;				
				// ========= Used by Unity to provide offset/tiling =========
				float4 _MainTex_ST;
				float4 _NormalMap_ST;
				// ========= Used by Unity to provide Light Color =========
				fixed4 _LightColor0;
				
				// ========= VERTEX to FRAGMENT Struct =========
				struct v2f {
					float4 mvpPos : SV_POSITION;
					float2 UV : TEXCOORD0;
					float3 tsLight : TEXCOORD1;
					float3 tsView : TEXCOORD2;
					float3 tsNormal : TEXCOORD3;
					
					float3 Attenuation : TEXCOORD4;
				};
				
				// ========= VERTEX SHADER =========
				v2f vert(appdata_tan v)	// use appdata_tan to have access to the vertex tangent
				{
					v2f o;
					
					float4 wsPos = mul(v.vertex, _Object2World);
					// ========= NORMAL, TANGENT, BITANGENT =========
					fixed4 normal   = normalize(mul(float4(v.normal, 0.0), _Object2World));
					fixed4 tangent  = normalize(mul(v.tangent,             _Object2World));
					fixed3 binormal = normalize(cross(v.tangent.xyz, v.normal.xyz) * tangent.w);
										
					// If Directional, then w = 0, otherwise 1
					float isNonDirLight = _WorldSpaceLightPos0.w;					
					
					// Calculate light direction and view direction
					float3 lightDir = _WorldSpaceLightPos0.xyz - (wsPos.xyz * isNonDirLight);
					float3 viewDir  = _WorldSpaceCameraPos - wsPos.xyz;
					float3 attenuation = (-lightDir / _LightRange) * isNonDirLight;
					// Create worldToTangentSpace transformation matrix					
					fixed3x3 ws2tsMatrix = fixed3x3(tangent.xyz, binormal, normal.xyz);
					// ========= SETUP OUTPUT =========
					o.mvpPos = mul(UNITY_MATRIX_MVP, v.vertex);	// Position					
					o.UV     = v.texcoord;						// Texture coordinates
					o.tsNormal = mul(ws2tsMatrix, normal.xyz);	// Normal
					o.tsLight  = mul(ws2tsMatrix, lightDir);	// Light
					o.tsView   = mul(ws2tsMatrix, viewDir);		// View
					o.Attenuation = attenuation;				// Attenuation
												
					return o;
				}
				
				// ========= FRAGMENT SHADER =========
				fixed4 frag(v2f i) : COLOR
				{
					// ========= SAMPLE FROM TEXTURES =========
					fixed4 texCol = tex2D(_MainTex, TRANSFORM_TEX(i.UV, _MainTex));	// read color from _Main
					fixed4 normalFromMap = tex2D(_NormalMap, TRANSFORM_TEX(i.UV, _NormalMap)); // Read normal from _Normal
					// ========= UNPACK NORMAL FROM NORMAL MAP =========
					// Unity stores x and y from normal in a and g, if texture type is "Normal Map"
					fixed3 bumpNormal;
					bumpNormal.xy = normalFromMap.ag * 2 - 1; // Expand from [0,1] to [-1,1]
					bumpNormal.z = sqrt(1 - saturate(dot(bumpNormal.xy, bumpNormal.xy)));	// Normalvector has length 1, so the z component can be calulcated from x and y
					bumpNormal = bumpNormal * _Bumpiness;	// Add Bumpiness factor
					// ========= CALCULATE LIGHTING VARIABLES =========
					fixed3 Normal   = normalize(i.tsNormal + bumpNormal);			// Combined Normal 
					fixed3 LightDir = normalize(i.tsLight);							// normalized light direction
					fixed3 ViewDir  = normalize(i.tsView);							// normalized view direction
					float nl = dot(Normal, LightDir);								// N dot L
					fixed shadow = saturate((1.0f * _SelfShadowingOff) + (4 * nl)); // Compute self shadowing term
					float att = dot(i.Attenuation, i.Attenuation);					// Compute Light Attenuation
					// ========= CALCULATE COLORS =========
					// Ambient color
					fixed4 ambientCol = saturate(UNITY_LIGHTMODEL_AMBIENT * _Color);
					// Diffuse Color
					fixed4 diffuseCol = _Color * _LightColor0 * max(0.0, nl);
					#ifndef NO_SPECULAR
					// Specular Color
					fixed3 reflect = normalize(2 * nl * Normal - LightDir);
					fixed4 specularCol = _SpecColor * _LightColor0 * pow(saturate(dot(reflect, ViewDir)), _Shininess);
					#endif
					// ========= CALCULATE FINAL COLOR =========
					fixed4 col = {0.0f, 0.0f, 0.0f, 0.0f};
					col += ambientCol;
					#ifdef NO_SPECULAR
					col += (shadow * (diffuseCol)) * (1 - att);
					#else
					col += (shadow * (diffuseCol + specularCol)) * (1 - att);
					#endif
					col *= texCol;
					
					return col;
				}
			ENDCG
		}
		
		Pass {
			// Pass for additional light sources
			Tags { "LightMode" = "ForwardAdd" }
			Blend One One	// Additive Blending
			
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#include "UnityCG.cginc"
				
				#define NO_SPECULAR
				
				// ========= PARAMETERS =========
				fixed4 _Color;
				fixed4 _SpecColor;
				float  _Shininess;
				float  _Bumpiness;
				int    _SelfShadowingOff;
				float  _LightRange;
				
				sampler2D _MainTex;
				sampler2D _NormalMap;				
				// ========= Used by Unity to provide offset/tiling =========
				float4 _MainTex_ST;
				float4 _NormalMap_ST;
				// ========= Used by Unity to provide Light Color =========
				fixed4 _LightColor0;
				
				// ========= VERTEX to FRAGMENT Struct =========
				struct v2f {
					float4 mvpPos : SV_POSITION;
					float2 UV : TEXCOORD0;
					float3 tsLight : TEXCOORD1;
					float3 tsView : TEXCOORD2;
					float3 tsNormal : TEXCOORD3;
					
					float3 Attenuation : TEXCOORD4;
				};
				
				// ========= VERTEX SHADER =========
				v2f vert(appdata_tan v)	// use appdata_tan to have access to the vertex tangent
				{
					v2f o;
					
					float4 wsPos = mul(v.vertex, _Object2World);
					// ========= NORMAL, TANGENT, BITANGENT =========
					fixed4 normal   = normalize(mul(float4(v.normal, 0.0), _Object2World));
					fixed4 tangent  = normalize(mul(v.tangent,             _Object2World));
					fixed3 binormal = normalize(cross(v.tangent.xyz, v.normal.xyz) * tangent.w);
										
					// If Directional, then w = 0, otherwise 1
					float isNonDirLight = _WorldSpaceLightPos0.w;					
					
					// Calculate light direction and view direction
					float3 lightDir = _WorldSpaceLightPos0.xyz - (wsPos.xyz * isNonDirLight);
					float3 viewDir  = _WorldSpaceCameraPos - wsPos.xyz;
					float3 attenuation = (-lightDir / _LightRange) * isNonDirLight;								
					// Create worldToTangentSpace transformation matrix					
					fixed3x3 ws2tsMatrix = fixed3x3(tangent.xyz, binormal, normal.xyz);
					// ========= SETUP OUTPUT =========
					o.mvpPos = mul(UNITY_MATRIX_MVP, v.vertex);	// Position					
					o.UV     = v.texcoord;						// Texture coordinates
					o.tsNormal = mul(ws2tsMatrix, normal.xyz);	// Normal
					o.tsLight  = mul(ws2tsMatrix, lightDir);	// Light
					o.tsView   = mul(ws2tsMatrix, viewDir);		// View
					o.Attenuation = attenuation;				// Attenuation
												
					return o;
				}
				
				// ========= FRAGMENT SHADER =========
				fixed4 frag(v2f i) : COLOR
				{
					// ========= SAMPLE FROM TEXTURES =========
					fixed4 texCol = tex2D(_MainTex, TRANSFORM_TEX(i.UV, _MainTex));	// read color from _Main
					fixed4 normalFromMap = tex2D(_NormalMap, TRANSFORM_TEX(i.UV, _NormalMap)); // Read normal from _Normal
					// ========= UNPACK NORMAL FROM NORMAL MAP =========
					// Unity stores x and y from normal in a and g, if texture type is "Normal Map"
					fixed3 bumpNormal;
					bumpNormal.xy = normalFromMap.ag * 2 - 1; // Expand from [0,1] to [-1,1]
					bumpNormal.z = sqrt(1 - saturate(dot(bumpNormal.xy, bumpNormal.xy)));	// Normalvector has length 1, so the z component can be calulcated from x and y
					bumpNormal = bumpNormal * _Bumpiness;	// Add Bumpiness factor
					// ========= CALCULATE LIGHTING VARIABLES =========
					fixed3 Normal   = normalize(i.tsNormal + bumpNormal);			// Combined Normal 
					fixed3 LightDir = normalize(i.tsLight);							// normalized light direction
					fixed3 ViewDir  = normalize(i.tsView);							// normalized view direction
					float nl = dot(Normal, LightDir);								// N dot L
					fixed shadow = saturate((1.0f * _SelfShadowingOff) + (4 * nl)); // Compute self shadowing term
					float att = dot(i.Attenuation, i.Attenuation);					// Compute Light Attenuation
					// ========= CALCULATE COLORS =========
					// Ambient color
					fixed4 ambientCol = saturate(UNITY_LIGHTMODEL_AMBIENT * _Color);
					// Diffuse Color
					fixed4 diffuseCol = _Color * _LightColor0 * max(0.0, nl);
					#ifndef NO_SPECULAR
					// Specular Color
					fixed3 reflect = normalize(2 * nl * Normal - LightDir);
					fixed4 specularCol = _SpecColor * _LightColor0 * pow(saturate(dot(reflect, ViewDir)), _Shininess);
					#endif
					// ========= CALCULATE FINAL COLOR =========
					fixed4 col = {0.0f, 0.0f, 0.0f, 0.0f};
					//col += ambientCol;
					#ifdef NO_SPECULAR
					col += (shadow * (diffuseCol)) * (1 - att);
					#else
					col += (shadow * (diffuseCol + specularCol)) * (1 - att);
					#endif
					col *= texCol;
					
					return col;
				}
			ENDCG
		}
	} 
	//FallBack "VertexLit"		// Do not use fallback during development
}
