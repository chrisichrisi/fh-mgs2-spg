﻿using UnityEngine;
using System.Collections;

public class DebugMovement : MonoBehaviour {
	// Exposed Members
	public float moveSpeed = 1.0f;
	public float moveSpeedFast = 10.0f;
	
	// Private Members
	private bool updateMouse;
	
	void Start()
	{
		Screen.lockCursor = true;
		this.updateMouse = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		float curMoveSpeed = moveSpeed * Time.deltaTime;
		if (Input.GetKey (KeyCode.LeftShift)) {
			curMoveSpeed = moveSpeedFast * Time.deltaTime;
		}

		
		Vector3 move = Vector3.zero;
		if (Input.GetKey (KeyCode.W)) { // Move forward
			move.z += curMoveSpeed;
		}
		if (Input.GetKey (KeyCode.S)) { // Move backwards
			move.z -= curMoveSpeed;
		}
		if (Input.GetKey (KeyCode.A)) { // Move left
			move.x -= curMoveSpeed;
		}
		if (Input.GetKey (KeyCode.D)) { // Move right
			move.x += curMoveSpeed;
		}
		if (Input.GetKey (KeyCode.E)) { // Move up
			move.y += curMoveSpeed;
		}
		if (Input.GetKey (KeyCode.Q)) { // Move down
			move.y -= curMoveSpeed;
		}
		
		// Apply position
		this.transform.Translate (move);
		
		UpdateMouse ();
	}

	void UpdateMouse()
	{
		if(Input.GetKeyUp(KeyCode.LeftControl))
		{
			this.updateMouse = true;
			Screen.lockCursor = true;
		}
		else if(Input.GetKeyDown(KeyCode.LeftControl))
		{
			this.updateMouse = false;
			Screen.lockCursor = false;
		}
				
		if(this.updateMouse)
		{
			Vector3 mouseOffset = new Vector3(-Input.GetAxis("Mouse Y"), Input.GetAxis("Mouse X"), 0f);
			this.transform.Rotate(mouseOffset, Space.Self);
			this.transform.LookAt(camera.transform.position + camera.transform.forward, Vector3.up);
		}
	}
}
