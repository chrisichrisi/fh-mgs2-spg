﻿using UnityEngine;
using System.Collections;

public interface IGUIBase
{
	// Public Members	
	Material ShaderMaterial { get; set; }
	Light ShaderLight { get; set; }
	void Setup();
}


public abstract class GUIBase<T> : MonoBehaviour, IGUIBase {

	// Exposed Members
	public T value;
	public float positionY;

	
	// Public Members	
	public Material ShaderMaterial { get; set; }
	public Light ShaderLight { get; set; }

	
	protected Rect position = new Rect(25, 75, 100, 20);

	protected string labelDesc = "";
	protected string label = "";

	// Private Members
	private const int labelControlYDistance = 25;
	private Rect labelPosition = new Rect (25, 50, 200, 50);

	public void Setup()
	{
		this.OnSetup ();
		this.labelPosition.x = this.position.x;
		this.labelPosition.y = this.position.y - GUIBase<T>.labelControlYDistance;
	}


	protected void Update()
	{
		if(!this.enabled) { return; }
		
		this.position.y = this.positionY;
		this.labelPosition.y = this.position.y - GUIBase<T>.labelControlYDistance;
		this.OnUpdate ();
	}

	void OnGUI()
	{
		if(!this.enabled) { return; }
		
		T val = this.DrawGui ();
		this.SetIfNew(val);
		GUI.Label (this.labelPosition, this.label);
	}

	// Methods
	public void SetIfNew(T newValue)
	{
		//if (this.value == newValue) { return; }
		if (this.value.Equals(newValue)) { return; }
		this.Set (newValue);
	}
	
	public void Set(T newValue)
	{
		this.value = newValue;
		this.OnSetValue ();
		this.label = this.labelDesc + ": " + this.FormatValue ();
	}

	
	protected virtual void OnSetup() { }
	protected virtual void OnUpdate() { }
	protected virtual string FormatValue() { return ""; }
	protected abstract T DrawGui();
	protected abstract void OnSetValue();


}
