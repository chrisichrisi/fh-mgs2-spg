﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GUIRoot : MonoBehaviour {

	// Exposed Members
	public Material shaderMaterial;
	public Light shaderLight;

	// Private Members	
	private List<IGUIBase> guis = new List<IGUIBase>();

	// Use this for initialization
	void Start () {
		Type guiInterface = typeof(IGUIBase);
		
		MonoBehaviour[] scripts = this.GetComponents<MonoBehaviour>();
		foreach(MonoBehaviour script in scripts)
		{
			if(script == this) { continue; }
			var t = script.GetType();
			if(guiInterface.IsAssignableFrom(t)) {				
				this.guis.Add(script as IGUIBase);
			}
		}
		
		foreach(IGUIBase g in this.guis)
		{
			// Set Material
			g.ShaderMaterial = this.shaderMaterial;
			// Set Light
			g.ShaderLight = this.shaderLight;
			g.Setup();
		}
	}

}
