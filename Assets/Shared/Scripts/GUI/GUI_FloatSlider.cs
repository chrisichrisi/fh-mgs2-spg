﻿using UnityEngine;
using System.Collections;

public class GUI_FloatSlider : GUIBase<float> {

	// Exposed Members
	public float minValue;
	public float maxValue;

	// Protected Members
	protected string shaderParameterName;

	// Methods
	protected override float DrawGui()
	{
		return GUI.HorizontalSlider (this.position, this.value, this.minValue, this.maxValue);
	}
		
	protected override string FormatValue()
	{
		return this.value.ToString ("#.00");
	}

	protected override void OnSetValue()
	{
		this.ShaderMaterial.SetFloat (this.shaderParameterName, this.value);
	}

	protected float GetShaderValue()
	{
		return this.ShaderMaterial.GetFloat (this.shaderParameterName);
	}
}
