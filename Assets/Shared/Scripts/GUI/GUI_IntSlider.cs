﻿using UnityEngine;
using System.Collections;

public class GUI_IntSlider : GUIBase<int> {

	// Exposed Members
	public int minValue;
	public int maxValue;
	
	// Protected Members
	protected string shaderParameterName;
	
	// Methods
	protected override int DrawGui()
	{
		
		float f = GUI.HorizontalSlider (this.position, this.value, this.minValue, this.maxValue);
		return Mathf.RoundToInt(f);
	}
	
	protected override string FormatValue()
	{
		return this.value.ToString ("#");
	}
	
	protected override void OnSetValue()
	{
		this.ShaderMaterial.SetInt (this.shaderParameterName, this.value);
	}
	
	protected int GetShaderValue()
	{
		return this.ShaderMaterial.GetInt (this.shaderParameterName);
	}
}
